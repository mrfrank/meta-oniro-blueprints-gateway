# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

SUMMARY = "Gateway blueprint: OpenThread node"
DESCRIPTION = "Zephyr based OpenThread node image"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_DEST = "destsuffix=git/apps/openthread-node"
SRC_OPT_NAME = "name=otnode"
SRC_OPT_BRANCH = "branch=main"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_NAME};${SRC_OPT_BRANCH}"
SRC_URI += "git://gitlab.eclipse.org/eclipse/oniro-blueprints/transparent-gateway/openthread-node-zephyr.git;${SRC_OPTIONS}"

SRCREV_otnode = "c74be639203529b2eb22436562ccb2338add0111"

ZEPHYR_SRC_DIR = "${S}/apps/openthread-node"

# The overlay config and OpenThread itself imposes some specific requirements
# towards the boards (e.g. flash layout and ieee802154 radio) so we need to
# limit to known working machines here.
COMPATIBLE_MACHINE = "(arduino-nano-33-ble)"
