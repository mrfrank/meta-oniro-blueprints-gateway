# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

require recipes-core/images/oniro-image-base.bb

IMAGE_INSTALL:append = " \
    packagegroup-thread-br \
    packagegroup-thread-client \
    tcpdump \
    networkmanager-softap-config \
    podman \
    podman-service \
 "
${APPDATA_PARTITION_SIZE} = "2G"
